<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "Ohh YEAHH BABY";
});

// $router->resources('users');
$router->group(['prefix' => 'users', 'middleware' => 'auth'], function () use ($router) {
    $router->get('', 'UsersController@index');
    $router->get('find/customer', 'UsersController@getCustomer');
    $router->post('', 'UsersController@store');
    $router->put('{id}', 'UsersController@update');
    $router->delete('delete/{id}', 'UsersController@delete');
    $router->get('{id}', 'UsersController@findById');
});
$router->group(['prefix' => 'roles', 'middleware' => 'auth'], function () use ($router) {
    $router->get('', 'RolesController@index');
    $router->get('find/all', 'RolesController@getAll');
    $router->post('', 'RolesController@store');
    $router->put('{id}', 'RolesController@update');
    $router->delete('{id}', 'RolesController@delete');
    $router->get('{id}', 'RolesController@findById');
});
$router->group(['prefix' => 'categories', 'middleware' => 'auth'], function () use ($router) {
    $router->get('', 'CategoriesController@index');
    $router->get('find/all', 'CategoriesController@getAll');
    $router->post('', 'CategoriesController@store');
    $router->put('{id}', 'CategoriesController@update');
    $router->delete('{id}', 'CategoriesController@delete');
    $router->get('{id}', 'CategoriesController@findById');
});
$router->group(['prefix' => 'products', 'middleware' => 'auth'], function () use ($router) {
    $router->get('', 'ProductsController@index');
    $router->get('find/productsSales', 'ProductsController@getAllProduct');
    $router->post('', 'ProductsController@store');
    $router->post('upload/productImage', 'ProductsController@upload');
    $router->post('add/variants', 'ProductsController@storeVariant');
    $router->put('update/variants/{id}', 'ProductsController@updateVariant');
    $router->delete('delete/variants/{id}', 'ProductsController@deleteVariant');
    $router->put('{id}', 'ProductsController@update');
    $router->delete('{id}', 'ProductsController@delete');
    $router->get('{id}', 'ProductsController@findById');
});
$router->group(['prefix' => 'sales', 'middleware' => 'auth'], function () use ($router) {
    $router->get('', 'SalesOrdersController@index');
    $router->post('', 'SalesOrdersController@store');
    $router->put('{id}', 'SalesOrdersController@update');
    $router->delete('{id}', 'SalesOrdersController@delete');
    $router->get('{id}', 'SalesOrdersController@findById');
});

$router->group(['prefix' => 'auth'], function ($router) {
    $router->post('login', 'AuthController@login');
    $router->get('me', 'AuthController@me');
    $router->post('refresh', 'AuthController@refresh');
    $router->get('logout', 'AuthController@logout');
});

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    protected $rules = [
        'name' => 'required',
        'status' => 'required',
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $req = $request->all();
        $page = !empty($req['page']) ? $req['page'] : 0;
        $keyword = !empty($req['q']) ? strtoupper($req['q']) : '';
        $perPage = $request->perPage ? $request->perPage : 10;
        // DB::enableQueryLog();

        $datas = Category::query();
        $datasCount = Category::query();

        if (!empty($keyword)) {
            $datas->orWhere(function ($query) use ($keyword) {
                $query->where('categories.name', 'LIKE', $keyword . '%');
            });
            $datasCount->orWhere(function ($query) use ($keyword) {
                $query->where('categories.name', 'LIKE', $keyword . '%');
            });

            // $datas->where('name','LIKE',$keyword.'%');
        }
        $colSort = (!empty($request['colSort']) ? $request['colSort'] : '');
        $sortDirectionCol = (!empty($request['sortDirectionCol']) ? $request['sortDirectionCol'] : 'ASC');
        if (empty($colSort)) {
            $datas->orderBy('categories.name', 'ASC');
        } else {
            $datas->orderBy($colSort, $sortDirectionCol);
        }
        if (!empty($perPage)) {
            $datas->limit($perPage);
        }
        if (!empty($page)) {
            if ($page == 1) {
                $page = 0;
            }
            $datas->offset($page);
        }

        $data = $datas->get();

        $count = $datasCount->count();
        if ($count > 0) {
            $totalPages = ceil($count / $perPage);
        } else {
            $totalPages = 0;
        }


        if (empty($datas)) {
            return response()->json([
                'message' => 'DATA NOT FOUND',
                'code' => 404
            ], 404);
        }

        // $sql = DB::getQueryLog();
        return response()->json([
            'data' => $data,
            'allData' => [],
            'totalPages' => $totalPages,
            // 'query' => $sql
        ], 200);
    }

    public function getAll()
    {
        $category = Category::where('status', '1')->get();
        return response()->json([
            'message' => 'success',
            'code'    => 200,
            'categories' => $category
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);


        $dataInput = $request->input();
        $category = new Category;
        $category->name = trim($dataInput['name']);
        $category->status = $dataInput['status'];
        $category->save();

        return response()->json([

            'category' => $category,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);

        $dataInput = $request->input();
        $category = Category::find($id);
        if (empty($category)) {
            return response()->json([
                'message' => 'CATEGORY NOT FOUND',
                'code' => 404
            ], 404);
        }
        $category->name = trim($dataInput['name']);
        $category->status = $dataInput['status'];
        $category->save();

        return response()->json([
            'category' => $category,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function delete($id)
    {
        $category = Category::find($id);
        if (empty($category)) {
            return response()->json([
                'message' => 'CATEGORY NOT FOUND',
                'code' => 404
            ], 404);
        }
        $category->delete();
        return response()->json([
            'category' => $category,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function findById($id)
    {
        $category = Category::find($id);
        if (empty($category)) {
            return response()->json([
                'message' => 'CATEGORY NOT FOUND',
                'code' => 404
            ], 404);
        }
        return response()->json([
            'category' => $category,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    //
}

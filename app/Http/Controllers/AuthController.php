<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login', 'refresh']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {

            $username = $request->username;
            $password = $request->password;

            if (empty($username) or empty($password)) {
                return response()->json(['status' => 'failed', 'message' => 'You must fill all fields'], 422);
            }

            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ]);

            $response =  $client->post('http://pos_app_be.test/v1/oauth/token', [
                "form_params" => [
                    'grant_type' => 'password',
                    'client_id' => '4',
                    'client_secret' => 'p6VAxUrUF8E6KLMkl4y8AgZngrQdkEyUdKs4N1GH',
                    'username' => $username,
                    'password' => $password,
                    'scope' => '',
                ]
            ]);

            $data = json_decode((string) $response->getBody(), true);
            $user = User::with(['role'])->where('username', $username)->first();
            return response()->json([
                'message' => 'success login',
                'status' => 'success',
                'data' => [
                    'expires_in'           => $data['expires_in'],
                    'access_token'         => $data['access_token'],
                    'refresh_token'        => $data['refresh_token'],
                    'token_type'           => $data['token_type'],
                    'users'                => $user
                ]
            ], 200);
        } catch (\Exception $e) {
            $code =  $e->getCode();
            switch ($code) {
                case '400':
                    return response()->json([
                        'message' => 'Username atau Password Salah',
                        'status' => 'failed',
                    ], 400);
                    break;
                case 401:
                    return response()->json([
                        'message' => 'Tidak Terdaftar',
                        'status' => 'failed',
                    ], 401);
                    break;

                default:
                    return response()->json([
                        'message' => $e->getMessage(),
                        'status' => 'failed',
                    ], 500);
                    break;
            }
        }

        // return $this->respondWithToken($token);
    }


    public function refresh(Request $request)
    {
        try {

            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ]);

            $response =  $client->post('http://pos_app_be.test/v1/oauth/token', [
                "form_params" => [
                    'grant_type' => 'refresh_token',
                    'client_id' => '4',
                    'client_secret' => 'p6VAxUrUF8E6KLMkl4y8AgZngrQdkEyUdKs4N1GH',
                    'refresh_token' => $request->refresh_token,
                    'scope' => '',
                ]
            ]);

            $data = json_decode((string) $response->getBody(), true);
            return response()->json([
                'message' => 'success login',
                'status' => 'success',
                'data' => [
                    'expires_in'           => $data['expires_in'],
                    'access_token'         => $data['access_token'],
                    'refresh_token'        => $data['refresh_token'],
                    'token_type'           => $data['token_type'],
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'status' => 'failed',
            ], 500);
        }

        // return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $getAuthuser = auth('api')->user();
        $user = \App\Models\User::with('role')->find($getAuthuser->id);
        return response()->json($user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    // public function refresh()
    // {
    //     return $this->respondWithToken(auth('api')->refresh());
    // }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}

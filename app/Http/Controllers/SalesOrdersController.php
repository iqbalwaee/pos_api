<?php

namespace App\Http\Controllers;
use App\Models\SalesOrder;
use App\Models\SalesOrdersItem;
use Illuminate\Http\Request;

class SalesOrdersController extends Controller
{

    protected $rules = [
        'sales_orders_details' => 'required'
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {   
        $salesOrder = SalesOrder::all();
        if(empty($salesOrder)){
            return response()->json([
                'message' => 'SALES ORDERS NOT FOUND',
                'code' => 404
            ],404);
        }
        return response()->json($salesOrder, 200);

    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
    

        $dataInput = $request->input();
        if(empty($dataInput['sales_orders_details'])){
            return response()->json([
                'message' => 'INVALID PARAMETER',
                'code' => 233
            ],233);
        }
        $salesOrder = new SalesOrder;
        $salesOrder->code = "";
        $salesOrder->customer_id  = $dataInput['customer_id'];
        
        $salesOrder->total_cost  = $dataInput['total_cost'];
        $salesOrder->total_amount  = $dataInput['total_amount'];
        $salesOrder->total_payment  = $dataInput['total_payment'];
        $salesOrder->debts_amount  = $dataInput['debts_amount'];
        $salesOrder->subtotal  = $dataInput['subtotal'];
        $salesOrder->order_status  = $dataInput['order_status'];
        $salesOrder->status  = $dataInput['status'];
        $salesOrder->user_id  = $dataInput['user_id'];
        $salesOrder->save();

        $salesOrderId = $salesOrder->id;
        $dataDetails = $dataInput['sales_orders_details'];
        foreach($dataDetails as $key => $dataDetail){
            $salesOrdersItem = new SalesOrdersItem;
            $salesOrdersItem->sales_order_id = $salesOrderId;
            $salesOrdersItem->product_id = $dataDetail['product_id'];
            $salesOrdersItem->product_name = $dataDetail['product_name'];
            $salesOrdersItem->product_unit = $dataDetail['product_unit'];
            $salesOrdersItem->cost = $dataDetail['cost'];
            $salesOrdersItem->price = $dataDetail['price'];
            $salesOrdersItem->discount = $dataDetail['discount'];
            $salesOrdersItem->qty = $dataDetail['qty'];
            $subtotalPrice = ($dataDetail['price'] - $dataDetail['discount']) * $dataDetail['qty'];
            $subtotalCost = ($dataDetail['cost']) * $dataDetail['qty'];
            $salesOrdersItem->subtotal_price = $subtotalPrice;
            $salesOrdersItem->subtotal_cost = $subtotalCost;
            $salesOrdersItem->save();
        }

        return response()->json([
            'salesOrder' => $salesOrder,
            'code' => 200,
            'message' => 'success'
        ],200);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, $this->rules);

        $dataInput = $request->input();
        if(empty($dataInput['sales_orders_details'])){
            return response()->json([
                'message' => 'INVALID PARAMETER',
                'code' => 233
            ],233);
        }
        
        $salesOrder = SalesOrder::find($id);
        if(empty($salesOrder)){
            return response()->json([
                'message' => 'SALES ORDER NOT FOUND',
                'code' => 404
            ],404);
        }
        $salesOrder->customer_id  = $dataInput['customer_id'];
        
        $salesOrder->total_cost  = $dataInput['total_cost'];
        $salesOrder->total_amount  = $dataInput['total_amount'];
        $salesOrder->total_payment  = $dataInput['total_payment'];
        $salesOrder->debts_amount  = $dataInput['debts_amount'];
        $salesOrder->subtotal  = $dataInput['subtotal'];
        $salesOrder->order_status  = $dataInput['order_status'];
        $salesOrder->status  = $dataInput['status'];
        $salesOrder->user_id  = $dataInput['user_id'];
        $salesOrder->save();
        $salesOrderId = $salesOrder->id;
        SalesOrdersItem::where('sales_order_id', '=',$salesOrderId)->delete();
        
        $dataDetails = $dataInput['sales_orders_details'];
        foreach($dataDetails as $key => $dataDetail){
            $salesOrdersItem = new SalesOrdersItem;
            $salesOrdersItem->sales_order_id = $salesOrderId;
            $salesOrdersItem->product_id = $dataDetail['product_id'];
            $salesOrdersItem->product_name = $dataDetail['product_name'];
            $salesOrdersItem->product_unit = $dataDetail['product_unit'];
            $salesOrdersItem->cost = $dataDetail['cost'];
            $salesOrdersItem->price = $dataDetail['price'];
            $salesOrdersItem->discount = $dataDetail['discount'];
            $salesOrdersItem->qty = $dataDetail['qty'];
            $subtotalPrice = ($dataDetail['price'] - $dataDetail['discount']) * $dataDetail['qty'];
            $subtotalCost = ($dataDetail['cost']) * $dataDetail['qty'];
            $salesOrdersItem->subtotal_price = $subtotalPrice;
            $salesOrdersItem->subtotal_cost = $subtotalCost;
            $salesOrdersItem->save();
        }
        
        return response()->json([
            'salesOrder' => $salesOrder,
            'code' => 200,
            'message' => 'success'
        ],200); 
    }

    public function delete($id)
    {
        $salesOrder = SalesOrder::find($id);
        if(empty($salesOrder)){
            return response()->json([
                'message' => 'SALES ORDER NOT FOUND',
                'code' => 404
            ],404);
        }
        $salesOrder->delete();
        SalesOrdersItem::where('sales_order_id', '=',$id)->delete();
        
        return response()->json([
            'salesOrder' => $salesOrder,
            'code' => 200,
            'message' => 'success'
        ],200);
    }

    public function findById($id)
    {
        $salesOrder = SalesOrder::with('salesOrdersItems','salesOrdersItems.product')->find($id);
        if(empty($salesOrder)){
            return response()->json([
                'message' => 'SALES ORDER NOT FOUND',
                'code' => 404
            ],404);
        }
        return response()->json([
            'salesOrder' => $salesOrder,
            'code' => 200,
            'message' => 'success'
        ],200);
    }

    //
}

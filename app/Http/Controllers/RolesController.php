<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RolesController extends Controller
{

    protected $rules = [
        'name' => 'required',
        'status' => 'required',
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $req = $request->all();
        $page = !empty($req['page']) ? $req['page'] : 0;
        $keyword = !empty($req['q']) ? strtoupper($req['q']) : '';
        $perPage = $request->perPage ? $request->perPage : 10;
        // DB::enableQueryLog();

        $datas = Role::query();
        $datasCount = Role::query();

        if (!empty($keyword)) {
            $datas->orWhere(function ($query) use ($keyword) {
                $query->where('roles.name', 'LIKE', $keyword . '%');
            });
            $datasCount->orWhere(function ($query) use ($keyword) {
                $query->where('roles.name', 'LIKE', $keyword . '%');
            });

            // $datas->where('name','LIKE',$keyword.'%');
        }
        $colSort = (!empty($request['colSort']) ? $request['colSort'] : '');
        $sortDirectionCol = (!empty($request['sortDirectionCol']) ? $request['sortDirectionCol'] : 'ASC');
        if (empty($colSort)) {
            $datas->orderBy('roles.name', 'ASC');
        } else {
            $datas->orderBy($colSort, $sortDirectionCol);
        }
        if (!empty($perPage)) {
            $datas->limit($perPage);
        }
        if (!empty($page)) {
            if ($page == 1) {
                $page = 0;
            }
            $datas->offset($page);
        }

        $data = $datas->get();

        $count = $datasCount->count();
        if ($count > 0) {
            $totalPages = ceil($count / $perPage);
        } else {
            $totalPages = 0;
        }


        if (empty($datas)) {
            return response()->json([
                'message' => 'DATA NOT FOUND',
                'code' => 404
            ], 404);
        }

        // $sql = DB::getQueryLog();
        return response()->json([
            'data' => $data,
            'allData' => [],
            'totalPages' => $totalPages,
            // 'query' => $sql
        ], 200);
    }

    public function getAll()
    {
        $roles = Role::where('status', '1')->get();
        return response()->json([
            'message' => 'Success',
            'code' => 200,
            'roles' => $roles
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);


        $dataInput = $request->input();
        $role = new Role;
        $role->name = trim($dataInput['name']);
        $role->status = $dataInput['status'];
        $role->save();

        return response()->json([

            'role' => $role,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);

        $dataInput = $request->input();
        $role = Role::find($id);
        if (empty($role)) {
            return response()->json([
                'message' => 'ROLE NOT FOUND',
                'code' => 404
            ], 404);
        }
        $role->name = trim($dataInput['name']);
        $role->status = $dataInput['status'];
        $role->save();

        return response()->json([
            'role' => $role,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function delete($id)
    {
        $role = Role::find($id);
        if (empty($role)) {
            return response()->json([
                'message' => 'ROLE NOT FOUND',
                'code' => 404
            ], 404);
        }
        $role->delete();
        return response()->json([
            'role' => $role,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function findById($id)
    {
        $role = Role::find($id);
        if (empty($role)) {
            return response()->json([
                'message' => 'ROLE NOT FOUND',
                'code' => 404
            ], 404);
        }
        return response()->json([
            'role' => $role,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    //
}

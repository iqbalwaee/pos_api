<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductVariant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{

    protected $rules = [
        'code'           => 'required|unique:products',
        'barcode'        => 'required|unique:products',
        'category_id'    => 'required',
        'name'           => 'required|unique:products',
        'cost'           => 'required',
        'unit_a'         => 'required',
        'price_unit_a_1' => 'required',
        'price_unit_a_2' => 'required',
        'price_unit_a_3' => 'required',
        'qty_unit_a'     => 'required',
        'status'         => 'required',
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $req = $request->all();
        $page = !empty($req['page']) ? $req['page'] : 0;
        $keyword = !empty($req['q']) ? strtoupper($req['q']) : '';
        $perPage = $request->perPage ? $request->perPage : 10;
        DB::enableQueryLog();

        $datas = Product::query();
        $datasCount = Product::query();

        if (!empty($keyword)) {
            $datas->orWhere(function ($query) use ($keyword) {
                $query->where('products.name', 'LIKE', $keyword . '%')
                    ->orWhere('products.code', 'LIKE', $keyword . '%');
            });
            $datasCount->orWhere(function ($query) use ($keyword) {
                $query->where('products.name', 'LIKE', $keyword . '%')
                    ->orWhere('products.code', 'LIKE', $keyword . '%');
            });

            // $datas->where('name','LIKE',$keyword.'%');
        }
        $colSort = (!empty($request['colSort']) ? $request['colSort'] : '');
        $sortDirectionCol = (!empty($request['sortDirectionCol']) ? $request['sortDirectionCol'] : 'ASC');
        if (empty($colSort)) {
            $datas->orderBy('products.name', 'ASC');
        } else {
            $datas->orderBy($colSort, $sortDirectionCol);
        }
        if (!empty($perPage)) {
            $datas->limit($perPage);
        }
        if (!empty($page)) {
            if ($page == 1) {
                $page = 0;
            }
            $datas->offset($page);
        }

        $data = $datas->get();

        $count = $datasCount->count();
        if ($count > 0) {
            $totalPages = ceil($count / $perPage);
        } else {
            $totalPages = 0;
        }


        if (empty($datas)) {
            return response()->json([
                'message' => 'DATA NOT FOUND',
                'code' => 404
            ], 404);
        }

        $sql = DB::getQueryLog();
        return response()->json([
            'data' => $data,
            'allData' => [],
            'totalPages' => $totalPages,
            'query' => $sql
        ], 200);
    }

    public function upload(Request $request)
    {
        $randomName = substr(strftime(time()), 2);
        $gambar = $request->file('gambar')->getClientOriginalName();
        $size = $request->file('gambar')->getSize();
        $name = explode('.', $gambar);
        $ext = $request->file('gambar')->getClientOriginalExtension();
        $uploaded = $request->file('gambar')->move('./public/upload_folder/', $name[0] . '-' . $randomName . "." . $ext);
        $file_upload = $name[0] . '-' . $randomName . "." . $ext;

        return response()->json([
            'filename'  => $file_upload,
            'fileformat'   => $ext,
            'filesize' => number_format($size / 1048576, 2) . "MB",
            'file_path' => env('APP_URL') . 'public/upload_folder/' . $file_upload
        ]);
    }

    public function getAllProduct(Request $request){
        $productName = $request->name;
        if ($productName == '' || $productName == null) {
            return response()->json([
                'allData' => [],
                'code' => 200,
                'message' => 'Success'
            ]);  
        }
        $product = Product::where('name','LIKE', strtoupper($productName) .'%')->get();

        return response()->json([
            'allData' => $product,
            'code' => 200,
            'message' => 'Success'
        ]);  
    }

    public function deleteVariant($id)
    {
        $product = ProductVariant::find($id);
        if (empty($product)) {
            return response()->json([
                'message' => 'PRODUCT NOT FOUND',
                'code' => 404
            ], 404);
        }
        $product->delete();
        return response()->json([
            'product' => $product,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function store(Request $request)
    {
        try {

            DB::beginTransaction();

            $this->validate($request, $this->rules);


            $dataInput = $request->input();
            $product = new Product;
            $product->code = trim(strtoupper($dataInput['code']));
            $product->barcode = trim(strtoupper($dataInput['barcode']));
            $product->name = trim(strtoupper($dataInput['name']));
            $product->cost = trim($dataInput['cost']);
            $product->category_id = $dataInput['category_id'];
            //UNIT A
            $product->unit_a = $dataInput['unit_a'];
            $product->qty_unit_a = $dataInput['qty_unit_a'];
            $product->price_unit_a_1 = $dataInput['price_unit_a_1'];
            $product->price_unit_a_2 = $dataInput['price_unit_a_2'];
            $product->price_unit_a_3 = $dataInput['price_unit_a_3'];
            //UNIT B
            $product->unit_b = $dataInput['unit_b'];
            $product->qty_unit_b = $dataInput['qty_unit_b'];
            $product->price_unit_b_1 = $dataInput['price_unit_b_1'];
            $product->price_unit_b_2 = $dataInput['price_unit_b_2'];
            $product->price_unit_b_3 = $dataInput['price_unit_b_3'];
            //UNIT C
            $product->unit_c = $dataInput['unit_c'];
            $product->qty_unit_c = $dataInput['qty_unit_c'];
            $product->price_unit_c_1 = $dataInput['price_unit_c_1'];
            $product->price_unit_c_2 = $dataInput['price_unit_c_2'];
            $product->price_unit_c_3 = $dataInput['price_unit_c_3'];
            $product->status = $dataInput['status'];
            $product->status_stock = $dataInput['status_stock'];
            $product->image = $dataInput['image'];
            $product->save();

            DB::commit();
            return response()->json([

                'product' => $product,
                'code' => 200,
                'message' => 'success'
            ], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'product' => null,
                'code' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function storeVariant(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->validate($request, [
                'product_id' => 'required',
                'name'       => 'required',
                'status'     => 'required'
            ]);

            $data = ProductVariant::create($request->all());

            DB::commit();
            return response()->json([
                'message' => 'Success Created',
                'code'    =>  201,
                'data'    => $data
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    =>  500,
                'data'    => null
            ]);
        }
    }

    public function updateVariant(Request $request, $id)
    {
        $this->validate($request, [
            'name'       => 'required',
            'status'     => 'required'
        ]);

        $find = ProductVariant::find($id);

        if ($find) {
            $find->update([
                'name' => $request->name,
                'status' => $request->status
            ]);

            return response()->json([
                'message' => 'Success Update Variant',
                'code' => 201,
                'data' => $find
            ], 201);
        } else {
            return response()->json([
                'message' => 'Failed Update Variant',
                'code' => 400,
                'data' => null
            ], 400);
        }
    }

    public function update(Request $request, $id)
    {
        $rules = $this->rules;
        $rules['code'] = $rules['code'] . ',code,' . $id;
        $rules['barcode'] = $rules['barcode'] . ',barcode,' . $id;
        $rules['name'] = $rules['name'] . ',name,' . $id;
        // dd($rules);
        $this->validate($request, $rules);

        $dataInput = $request->input();
        $product = Product::find($id);
        if (empty($product)) {
            return response()->json([
                'message' => 'PRODUCT NOT FOUND',
                'code' => 404
            ], 404);
        }
        $product->code = trim(strtoupper($dataInput['code']));
        $product->barcode = trim(strtoupper($dataInput['barcode']));
        $product->name = trim(strtoupper($dataInput['name']));
        $product->cost = trim($dataInput['cost']);
        $product->category_id = $dataInput['category_id'];
        //UNIT A
        $product->unit_a = $dataInput['unit_a'];
        $product->qty_unit_a = $dataInput['qty_unit_a'];
        $product->price_unit_a_1 = $dataInput['price_unit_a_1'];
        $product->price_unit_a_2 = $dataInput['price_unit_a_2'];
        $product->price_unit_a_3 = $dataInput['price_unit_a_3'];
        //UNIT B
        $product->unit_b = $dataInput['unit_b'];
        $product->qty_unit_b = $dataInput['qty_unit_b'];
        $product->price_unit_b_1 = $dataInput['price_unit_b_1'];
        $product->price_unit_b_2 = $dataInput['price_unit_b_2'];
        $product->price_unit_b_3 = $dataInput['price_unit_b_3'];
        //UNIT C
        $product->unit_c = $dataInput['unit_c'];
        $product->qty_unit_c = $dataInput['qty_unit_c'];
        $product->price_unit_c_1 = $dataInput['price_unit_c_1'];
        $product->price_unit_c_2 = $dataInput['price_unit_c_2'];
        $product->price_unit_c_3 = $dataInput['price_unit_c_3'];
        $product->status = $dataInput['status'];
        $product->status_stock = $dataInput['status_stock'];
        $product->save();

        return response()->json([
            'product' => $product,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function delete($id)
    {
        $product = Product::find($id);
        if (empty($product)) {
            return response()->json([
                'message' => 'PRODUCT NOT FOUND',
                'code' => 404
            ], 404);
        }
        $product->delete();
        return response()->json([
            'product' => $product,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function findById($id)
    {
        $product = Product::find($id);
        if (empty($product)) {
            return response()->json([
                'message' => 'PRODUCT NOT FOUND',
                'code' => 404
            ], 404);
        }
        return response()->json([
            'product' => $product,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    //
}

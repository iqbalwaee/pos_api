<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'phone' => 'required',
        'password' => 'required',
        'username' => 'required|unique:users'
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $req = $request->all();
        $page = !empty($req['page']) ? $req['page'] : 0;
        $keyword = !empty($req['q']) ? strtoupper($req['q']) : '';
        $perPage = $request->perPage ? $request->perPage : 10;
        // DB::enableQueryLog();

        $datas = User::query()->join('roles','roles.id','users.role_id');
        $datasCount = User::query();

        if (!empty($keyword)) {
            $datas->orWhere(function ($query) use ($keyword) {
                $query->where('users.name', 'LIKE', $keyword . '%')
                    ->orWhere('users.email', 'LIKE', $keyword . '%')
                    ->orWhere('users.username', 'LIKE', $keyword . '%');
            });
            $datasCount->orWhere(function ($query) use ($keyword) {
                $query->where('users.name', 'LIKE', $keyword . '%');
            });

            // $datas->where('name','LIKE',$keyword.'%');
        }
        $colSort = (!empty($request['colSort']) ? $request['colSort'] : '');
        $sortDirectionCol = (!empty($request['sortDirectionCol']) ? $request['sortDirectionCol'] : 'ASC');
        if (empty($colSort)) {
            $datas->orderBy('users.name', 'ASC');
        } else {
            $datas->orderBy($colSort, $sortDirectionCol);
        }
        if (!empty($perPage)) {
            $datas->limit($perPage);
        }
        if (!empty($page)) {
            if ($page == 1) {
                $page = 0;
            }
            $datas->offset($page);
        }

        $data = $datas->select('users.*','roles.name as role_name')->get();

        $count = $datasCount->count();
        if ($count > 0) {
            $totalPages = ceil($count / $perPage);
        } else {
            $totalPages = 0;
        }


        if (empty($datas)) {
            return response()->json([
                'message' => 'DATA NOT FOUND',
                'code' => 404
            ], 404);
        }

        // $sql = DB::getQueryLog();
        return response()->json([
            'data' => $data,
            'allData' => [],
            'totalPages' => $totalPages,
            // 'query' => $sql
        ], 200);
    }

    public function getCustomer(Request $request){
        try {
            $search = $request->search;
            $customer = User::where(function($q) use ($search){
                $q->where('role_id',2);
                if ($search != '' || $search != null) {
                    $q->where('name', 'LIKE', $search . '%');
                }
            })
            ->select('name as label','id','role_id')
            ->get();

            return response()->json([
                'message' => 'Success',
                'users' => $customer,
                'code' => 200,
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'code' => $e->getCode(),
                'users' => [],
                'message' => $e->getMessage()
            ]);
        }
    }

    public function store(Request $request)
    {
        try {
            $rules = $this->rules;
            $this->validate($request, $rules);

            $dataInput = $request->input();
            $user = new User;
            $user->name = trim($dataInput['name']);
            $user->email = trim($dataInput['email']);
            $user->username = trim($dataInput['username']);
            $user->phone = trim($dataInput['phone']);
            $user->role_id = $dataInput['role_id'];
            $user->password =  app('hash')->make($dataInput['password']);
            $user->status = $dataInput['status'];
            $user->save();

            return response()->json([
                'user'    => $user,
                'code'    => 200,
                'message' => 'success'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage(),
                'code'     => 500,
                'user'     => null
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $rules = $this->rules;
            // $rules['email'] = $rules['email'] . ',email,' . $id;
            // $this->validate($request, $rules);
            // $this->validate($request,[
            //     'name' => 'required',
            //     'email'=> 'required',
            //     'username'=> 'required',
            //     'phone'=> 'required',
            //     'role_id'=> 'required',
            // ]);

            $dataInput = $request->input();
            $user = User::find($id);
            if (empty($user)) {
                return response()->json([
                    'message' => 'USER NOT FOUND',
                    'code' => 404
                ], 404);
            }
            $user->name = trim($dataInput['name']);
            $user->email = trim($dataInput['email']);
            $user->username = trim($dataInput['username']);
            $user->phone = trim($dataInput['phone']);
            $user->role_id = $dataInput['role_id'];
            if (!empty($dataInput['password'])) {
                $user->password =  app('hash')->make($dataInput['password']);
            }
            $user->status = $dataInput['status'];
            $user->save();
            return response()->json([
                'user' => $user,
                'code' => 200,
                'message' => 'success update'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'user' => null,
                'code' => 422,
                'message' => $e->getMessage()
            ], 200);
        }
    }

    public function delete($id)
    {
        $user = User::find($id);
        if (empty($user)) {
            return response()->json([
                'message' => 'USER NOT FOUND',
                'code' => 404
            ], 404);
        }
        $user->delete();
        return response()->json([
            'user' => $user,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    public function findById($id)
    {
        $user = User::with('role')->find($id);
        if (empty($user)) {
            return response()->json([
                'message' => 'USER NOT FOUND',
                'code' => 404
            ], 404);
        }
        return response()->json([
            'user' => $user,
            'code' => 200,
            'message' => 'success'
        ], 200);
    }

    //
}

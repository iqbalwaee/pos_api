<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SalesOrdersItem extends Model
{
    protected $guarded = ['id'];
    public function salesOrder()
    {
        return $this->belongsTo(SalesOrder::class);
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}


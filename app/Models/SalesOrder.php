<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
    
    protected $guarded = ['id'];
    public function salesOrdersItems()
    {
        return $this->hasMany(SalesOrdersItem::class);
    }
    public function customer()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $table = 'products_variants';

    protected $guarded = ['id'];

    public function product()
    {
        return $this->belongsTo(ProductVariant::class);
    }
}
